# Inotify container

Useful as a sidecar container with a custom bash script.

Example pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  generateName: vault-
  labels:
    app.kubernetes.io/instance: vault
    app.kubernetes.io/name: vault
    component: server
    statefulset.kubernetes.io/pod-name: vault-0
    vault-version: 1.12.3
  name: vault-0
spec:
  affinity:
    podAntiAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchLabels:
              app.kubernetes.io/instance: vault
              app.kubernetes.io/name: vault
              component: server
          topologyKey: kubernetes.io/hostname
  containers:
  - image: hashicorp/vault:1.12.3
    name: vault
    env:
      - name: VAULT_CERT_DIR
        value: /vault/userconfig/tls
      # omitted
    # omitted
    volumeMounts:
      - mountPath: /vault/userconfig/tls
        name: vault-tls
        readOnly: true
      # omitted
  - args:
      - |
        while true; do
          inotifywait --recursive \
            --event modify,attrib,moved_to,moved_from,move,move_self \
            --event create,delete,delete_self,unmount \
            "$VAULT_CERT_DIR"
          vault_pid="$(pgrep -f '^vault server' | head -n 1)"
          if [ -z "${vault_pid:-}" ]; then
            echo "Failed to determine vault's PID!" >&2
            exit 1
          fi
          echo "Reloading vault's configuration ..."
          kill -HUP "$vault_pid"
        done
    command:
      - /bin/sh
      - -c
    env:
      - name: VAULT_CERT_DIR
        value: /vault/userconfig/tls
    image: registry.core.hpc.ethz.ch/util/inotify:latest
    imagePullPolicy: IfNotPresent
    name: cert-reloader
    volumeMounts:
      - mountPath: /vault/userconfig/tls
        name: vault-tls
        readOnly: true
  priority: 0
  restartPolicy: Always
  securityContext:
    fsGroup: 1000
    runAsGroup: 1000
    runAsNonRoot: true
    runAsUser: 100
  serviceAccount: vault
  serviceAccountName: vault
  shareProcessNamespace: true
  volumes:
    - name: vault-tls
      secret:
        defaultMode: 420
        secretName: vault-core-selfsigned
    # omitted
  # omitted
```

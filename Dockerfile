# renovate: datasource=docker depName=docker.io/alpine
ARG IMAGE_TAG=3.20@sha256:de4fe7064d8f98419ea6b49190df1abbf43450c1702eeb864fe9ced453c1cc5f
FROM alpine:${IMAGE_TAG}

# renovate: datasource=repology depName=alpine_3_20/bash versioning=semver-coerced
ARG BASH_VERSION=5.2.26
# renovate: datasource=repology depName=alpine_3_20/inotify-tools versioning=semver-coerced
ARG INOTIFY_VERSION=4.23.9
# renovate: datasource=repology depName=alpine_3_20/tini versioning=semver-coerced
ARG TINI_VERSION=0.19.0

# renovate: datasource=docker depName=docker.io/alpine
ARG IMAGE_TAG=3.20@sha256:de4fe7064d8f98419ea6b49190df1abbf43450c1702eeb864fe9ced453c1cc5f
ARG CREATED=""
ARG REVISION=""

# hadolint ignore=DL3019
RUN apk update \
 && apk upgrade \
 && apk add bash~="${BASH_VERSION}" \
            inotify-tools~="${INOTIFY_VERSION}" \
            tini~="${TINI_VERSION}" \
 && rm -rf /var/cache/apk/*
ENTRYPOINT ["/sbin/tini", "/bin/bash", "--"]

LABEL org.opencontainers.image.source="https://gitlab.ethz.ch/hpc-registry/inotify"
LABEL org.opencontainers.image.url="https://gitlab.ethz.ch/hpc-registry/inotify"
LABEL org.opencontainers.image.revision="${REVISION}"
LABEL org.opencontainers.image.base="docker.io/alpine:${IMAGE_TAG}"
LABEL org.opencontainers.image.authors="Michal Minář <michal.minar@id.ethz.ch>"
LABEL org.opencontainers.image.licenses="AGPL-3.0-or-later"
LABEL org.opencontainers.image.created="${CREATED}"
